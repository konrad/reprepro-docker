#!/usr/bin/expect
set timeout 2
set passphrase env(GPG_PASSWORD)

spawn reprepro -b [lindex $argv 0] [lindex $argv 1] [lindex $argv 2] [lindex $argv 3]
expect {
        "*passphrase:*" {
                send -- "$passphrase\r"
        }
}
# expect {
#         "*passphrase:*" {
#                 send -- "$passphrase\r"
#         }
# }
# interact
