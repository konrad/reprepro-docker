FROM debian
RUN apt-get update -y && \
apt-get upgrade -y && \
apt-get install reprepro expect make git -y 
ADD reprepro_expect.sh /usr/local/bin/reprepro_expect
RUN chmod +x /usr/local/bin/reprepro_expect
